<?
function fecha($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4);
}

function fecha_en($value) { // fecha de YYYY/MM/DD a DD/MM/YYYY
 if ( ! empty($value) ) return substr($value,5,2) ."/". substr($value,8,2) ."/". substr($value,0,4);
}

function fechahora($value) { // fecha de 'YYYY-MM-DD HH:MM:SS' a 'DD-MM-YYYY HH:MM:SS'
 if ( ! empty($value) ) return substr($value,8,2) ."/". substr($value,5,2) ."/". substr($value,0,4)." ".substr($value,11,8);
}

function fecha_sql($value) { // fecha de DD/MM/YYYY a YYYYY/MM/DD
 return substr($value,6,4) ."-". substr($value,3,2) ."-". substr($value,0,2);
}

function fechahora_sql($value) { // fecha de 'DD-MM-YYYY HH:MM:SS' a 'YYYY-MM-DD HH:MM:SS'
 return substr($value,6,4) ."-". substr($value,3,2) ."-". substr($value,0,2)." ".substr($value,11,8);
}

function tiempo($value) { // de 'YYYY-MM-DD HH:MM:SS' a 'HH:MM:SS'
 return substr($value,11,8);
}

function numero($value,$dec=0) {
 if ( ! empty($value) ) return number_format($value, 2, ',', '.');
}

function value($colName, $type, $method = '', $reference = 0) {
	global $$colName;
	global $$reference;
	if ($reference != ""){ $colName = $reference; }

	switch ($method) {
		case '':
			// por los check
			if ( isset($_REQUEST["$colName"]) ) {$val = $_REQUEST["$colName"]; } else { $val = "";}
			$value = $val;
			break;
		case "post":
			if ( isset($_POST["$colName"]) ) {$val = $_POST["$colName"]; } else { $val = "";}
			$value = $val;
			break;
		case "get":
			if ( isset($_GET["$colName"]) ) {$val = $_GET["$colName"]; } else { $val = "";}
			$value = $val;
			break;
		case "session":
			$value = $_SESSION["$colName"];
			break;
		case "var":
			$value = $$colName;
			break;
		case "value":
			$value = $reference;
			break;
	}
	$value = (!get_magic_quotes_gpc()) ? addslashes($value) : $value;
	switch ($type) {
		case "text":
			$value = ($value != "") ? "'" . $value . "'" : "NULL";
			break;
		case "long":
		case "int":
		  $value = ($value != "") ? intval(str_replace(",", ".", str_replace(".", "", $value))) : "0";
		  break;
		case "double":
		  $value = ($value != "") ? "'" . str_replace(array(".",","), ".", $value ) . "'" : "NULL";
		  break;
		case "date":
		  $value = ($value != "") ? "'" . fecha_sql($value) . "'" : "NULL";
		  break;
		case "datetime":
		  $value = ($value != "") ? "'" . fechahora_sql($value) . "'" : "NULL";
		  break;
		case "now":
		  $value = 'now()';
		  break;
	}
	return $value;
}

class insert {
    var $table;
	var $columns;
	var $sql;

	function table($table)
	{
	 $this->table = $table;
	}
	function addColumn($colName, $type, $method = '', $reference = 0, $default = '') {
     $this->columns[] = array ( "colName" => $colName, "type" => $type, "method" => $method, "reference" => $reference);
	}
	
	function sql(){
	$sql = "INSERT INTO ". $this->table ." (";
	for ($i = 0; $i < sizeof($this->columns); $i = $i + 1) {
	  $sql = $sql . $this->columns[$i]['colName'].",";
	};
	$sql = substr($sql, 0, -1).") VALUES (";

	reset($this->columns);

	for ($i = 0; $i < sizeof($this->columns); $i = $i + 1) {
	  $sql = $sql . value($this->columns[$i]['colName'],$this->columns[$i]['type'],$this->columns[$i]['method'],$this->columns[$i]['reference']).",";
	};

	$sql = substr($sql, 0, -1).")";

	return $sql;
	}
};// class


class update {
    var $table;
	var $columns;
	var $field_id;
	var $id;
	var $sql;

	function table($table){
	 $this->table = $table;
	}
	function addColumn($colName, $type, $method = 0, $reference = 0, $default = '') {
     $this->columns[] = array ( "colName" => $colName, "type" => $type, "method" => $method, "reference" => $reference);
	}
	function field_id($field_id){
	 $this->field_id = $field_id;
	}
	function id($id){
	 $this->id = $id;
	}
	function sql(){
	$sql = "UPDATE ". $this->table ." SET ";
	for ($i = 0; $i < sizeof($this->columns); $i = $i + 1) {
	  $sql = $sql . $this->columns[$i]['colName']."=".value($this->columns[$i]['colName'],$this->columns[$i]['type'],$this->columns[$i]['method'],$this->columns[$i]['reference']).",";
	};
	$sql = substr($sql, 0, -1)." WHERE $this->field_id = '$this->id'";
	return $sql;
	}
};// class

function select($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select2($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2) 
{
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
	  if ($selecione == 3) {$ret.="<option value='0'>$selecione2</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select3($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  if ($selecione == 3) {$ret.="<option value='$selecione3'>$selecione2</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

function select4($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name." disabled>";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select5($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name.">";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
      if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};
function select6($field_value, $field_label, $field_selected, $sql, $control_name, $selecione=0,$selecione2,$selecione3) {
      global $Conn; 
      $rs = $Conn->query($sql);
	  $ret="<select class='text-peq' name=".$control_name." onClick='validar();'>";
	  if ($selecione == 1) {$ret.="<option value=''>Seleccione</option>";};
	  if ($selecione == 2) {$ret.="<option value='0'>Seleccione</option>";};
 	  //if ($selecione == 3) {$ret.="<option value='0'>TODOS</option>";};
      while ($row_rs = $rs->fetch_assoc()) {;
        if ( $row_rs[$field_value] == $field_selected ) {;
         $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'" selected>'.htmlspecialchars($row_rs[$field_label]).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($row_rs[$field_value]).'">'.htmlspecialchars($row_rs[$field_label]).'</option>';
        };
      };
     $rs->close();
     $ret.="</select>";
	 print $ret;
};

function select_array($array, $field, $control_name) {
	  $ret="<select class='text-peq' name=".$control_name.">";
	  foreach ($array as $value) {
        if ( $value == $field ) {;
         $ret.='<option value="'.htmlspecialchars($value).'" selected>'.htmlspecialchars($value).'</option>';
        } else {;
          $ret.='<option value="'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</option>';
        };
	  };
	  $ret.="</select>";
	 print $ret;
};

function select_num($c_to, $c_name, $c_value) {
	$ret="<select class='text-peq' name=".$c_name.">";
	for ($i = 1; $i <= $c_to; $i++) {;
       if ($c_value == $i) {
	    $ret.="<option value=\"$i\"selected>$i</option>";
		} else {
		$ret.="<option value=\"$i\">$i</option>";
		}
    };
	$ret.="</select>";
	print $ret;
};

function check($name,$est){
?>
<input type="checkbox" name="<? echo $name ?>" value="1" <? if ($est == "1") echo "checked" ?>>
<?
}

function check_img($est){
  switch ($est) {
    	   case "": echo '<img src="img_sis/op_off.gif" width="15" height="15">';
    break; case "1": echo '<img src="img_sis/op_on.gif" width="15" height="15">';
  }
}


function boton($cod_modulo,$nom_menu,$archivo){
$ret = 
	'<div class="box">
	<table width="100" height="90" border="0" cellpadding="0" cellspacing="0" style="cursor: pointer;" onclick="javascript:window.location=\''.$archivo.'\'">
        <tr>
          <td height="45" valign="bottom"><div align="center"><img src="img_sis/icons/'.$cod_modulo.'.png" class="icon"/></div></td>
        </tr>
        <tr>
          <td height="45"><div align="center" class="boton-text">'.nl2br($nom_menu).'</div></td>
        </tr>
  </table>
	</div>
	';
print $ret;
}


function btn_img($tipo,$url){
 switch ($tipo) {;
 case "view":
	echo '';
	break; 
 case "edit":
	echo '<a href="'.$url.'"><img src="images/edit.gif" alt="Editar" width="16" height="16" border="0"></a>';
	break;
 case "del":
	echo '<a href="javascript:;" onClick="confirmar(\'Seguro de borrar\',\''.$url.'\'); return self.rValue"><img src="images/delete.gif" alt="Borrar" width="15" height="15" border="0"></a>';
	break;
 case "save":
	echo '<input type="image" class="no-br" src="img_sis/ico_save.gif" alt="Guardar" width="15" height="15" border="0">';
	break; 
 }
}

function btn($tipo,$url,$accion=0){ // Accion 0=location / 1=Form url = form name / 2 JS ** 3 reset **terminar**
switch ($tipo) {;
 case "add":
	$icon = 'add';
	$name = 'Agregar';
	break; 
 case "edit":
	$icon = 'edit';
	$name = 'Editar';
	break;
 case "del":
	$icon = 'delete';
	$name = 'Borrar';
	break;
 case "save":
	$icon = 'delete';
	$name = 'Borrar';
	break; 
 case "ok":
	$icon = 'ok';
	$name = 'Aceptar';
	break; 
 case "cancel":
	$icon = 'cancel';
	$name = 'Cancelar';
	break; 
 case "search":
	$icon = 'search';
	$name = 'Buscar';
	break; 
 case "show_all":
	$icon = 'list';
	$name = 'Mostrar todo';
	break;
 case "maestro":
	$icon = 'back';
	$name = 'Volver a la p�gina maestra';
	break;
 }
switch ($accion) {;
 case 0:
	$js = "window.location='$url'";
	break; 
 case 1:
	$js = "window.document.$url.submit()";
	break;
 case 2:
	$js = $url;
	break;
 }
echo '<table style="cursor: pointer;" class="btn_bg" onClick="javascript:'.$js.'" border="0" cellpadding="0" cellspacing="0">
		<tr>
		  <td style="padding: 0px;" align="right"><img src="img_sis/bt_left.gif" alt="" width="4" height="21" style="border-width: 0px;" /></td>
		  <td class="btn_bg"><img src="images/'.$icon.'.gif" width="16" height="16" /></td>
		  <td class="btn_bg" nowrap style="padding: 0px 4px;">'.$name.'</td>
		  <td style="padding: 0px;" align="left"><img src="img_sis/bt_right.gif" alt="" width="4" height="21" style="border-width: 0px;" /></td>
		</tr>
	  </table>';
}

function tit($tabla){
	global $ConnSys;
	$Conn = new mysqli($ConnSys["server"], $ConnSys["user"], $ConnSys["pass"], $ConnSys["db"]);
	$rs = $Conn->query("SELECT * FROM modulos WHERE tabla = '$tabla'");
	$row_rs = $rs->fetch_assoc();
	if ( $rs->num_rows == 1) {
	  echo '<img src="img_sis/icons/'.$row_rs['cod_modulo'].'.png" width="22" height="22" class="icon" /> '.$row_rs['nom_menu'];
	}
	$rs->close();
}

function detalle($div,$name,$file,$title,$form,$id) {

echo '
<table width="100%" border="0" cellspacing="1" cellpadding="3" class="ewTableTitle">
  <tr>
    <td>'.$title.'</td>
    <td align="right"><button type="button" onclick="'.$name.'Add();"><img src="images/add.gif" class="icon" /> Agregar</button></td>
  </tr>
</table>
<div id="'.$div.'list"></div>

<script type="text/javascript">
function '.$name.'List(){
new Ajax.Updater(\''.$div.'list\', \''.$file.'?id_ma='.$id.'\');
}

function '.$name.'Add(){
new Ajax.Updater(\''.$div.'list\', \''.$file.'?rsac=add&id_ma='.$id.'\');
}

function '.$name.'Added(){
var params = Form.serialize(\''.$form.'\');
new Ajax.Request(\''.$file.'?rsac=added\', { parameters: params, onComplete:'.$name.'List });
}

function '.$name.'Edit(id,id_ma){
new Ajax.Updater(\''.$div.'list\', \''.$file.'?id_ma=<?= $id ?>\', { parameters: \'id=\'+id } );
}

function '.$name.'Edited(){
var params = Form.serialize(\''.$form.'\');
new Ajax.Request(\''.$file.'?rsac=edited\', { parameters: params, onComplete:\''.$div.'list\'});
}

function '.$name.'Delete(id){
new Ajax.Request(\''.$file.'?rsac=deleted\', { parameters: \'id=\'+id , onComplete:\''.$div.'list\'});
}
'.$name.'List();
</script>';
}

?>