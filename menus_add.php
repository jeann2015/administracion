<?php
if (!isset($_SESSION)) 
{
	session_start();
	$var_cod_usu = $_SESSION['cod_usu'];
	$_SESSION['action']=0;
}
include ("db.php"); 

if($var_cod_usu==''){devolver();}

$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);

?>
<head>
<title><?php echo nombre_aplicacion(); ?></title>
</head>
<link href="assets/css/bootstrap.css" rel="stylesheet">
<body  class="">
<script type="text/javascript"><!--

function solo_numeros(e) 
{ 
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8) return true; 
    if (tecla==9) return true; 
    if (tecla==0) return true; 
    //patron =/[A-Za-z\s]/; 
    patron =/\d/; 
    te = String.fromCharCode(tecla); 
    return patron.test(te); 
} 

function validar_campos() 
{     
	var doc = document.form1;
	var var_codigo = doc.codigo.value.replace(/ /g, '');
	if(var_codigo==''){alert("SE REQUIERE LLENAR EL CAMPO DE CODIGO"); doc.codigo.focus(); return false; }
	var var_codigo_padre = doc.codigo_padre.value.replace(/ /g, '');
	if(var_codigo_padre==''){alert("SE REQUIERE LLENAR EL CAMPO CODIGO PADRE"); doc.codigo_padre.focus(); return false; }
	var var_descripcion = doc.descripcion.value.replace(/ /g, '');
	if(var_descripcion==''){alert("SE REQUIERE LLENAR EL CAMPO DE DESCRIPCION"); doc.descripcion.focus(); return false; }
	var var_archivo = doc.archivo.value.replace(/ /g, '');
	if(var_archivo==''){alert("SE REQUIERE LLENAR EL CAMPO DE ARCHIVO"); doc.archivo.focus(); return false; }
	var var_titulo = doc.titulo.value.replace(/ /g, '');
	if(var_titulo==''){alert("SE REQUIERE LLENAR EL CAMPO DE TITULO"); doc.titulo.focus(); return false; }
	var var_mensaje = doc.mensaje.value.replace(/ /g, '');
	if(var_mensaje==''){alert("SE REQUIERE LLENAR EL CAMPO DE MENSAJE"); doc.mensaje.focus(); return false; }
	var var_orden = doc.orden.value.replace(/ /g, '');
	if(var_orden==''){alert("SE REQUIERE LLENAR EL CAMPO DE ORDEN"); doc.orden.focus(); return false; }
	var var_tecla = doc.tecla.value.replace(/ /g, '');
	if(var_tecla==''){alert("SE REQUIERE LLENAR EL CAMPO DE TECLA"); doc.tecla.focus(); return false; }

}
</script>
<form name="form1" action="menus.php" method="post" >
<div  class="container">
	<div class="container-fluid">
  <table  border="1" class="table table-hover" align="center">
    <tr class="">
      <td align="center" colspan="2"><a  title="Regresar" href='menus_view.php'  class=' btn btn-success' ><i class='icon-chevron-left'></i>Regresar</a></td>
    </tr>
    <tr class="">
      <td colspan="2"><div align="center"></div><div align="left"><strong>Creacion de Menu</strong></div></td>
    </tr>
    <tr class="">
      <td ><div align="left">Codigo</div></td>
      <td ><div align="center">
        <input name="codigo" type="text" id="codigo" class="input-block-level" onKeypress="return solo_numeros(event);" />      
      </div></td>
      </tr>
    <tr class="">
      <td><div align="left">Codigo Padre:</div></td>
      <td>        <div align="center">
        <input name="codigo_padre" type="text" id="codigo_padre" class="input-block-level" onKeypress="return solo_numeros(event);" />      
      </div></td>
    </tr>
    <tr class="">
      <td>Descripcion:</td>
      <td>        <div align="center">
        <input name="descripcion" class="input-block-level" type="text" id="descripcion" />      
      </div></td>
    </tr>
    <tr class="">
      <td>Archivo:</td>
      <td>        <div align="center">
        <input name="archivo" type="text" id="archivo" class="input-block-level" />      
      </div></td>
    </tr>
    <tr class="">
      <td>Titulo:</td>
      <td>        <div align="center">
        <input name="titulo" type="text" id="titulo" class="input-block-level" />      
      </div></td>
    </tr>
    <tr class="">
      <td>Mensaje:</td>
      <td>        <div align="center">
        <input name="mensaje" type="text" id="mensaje" class="input-block-level" />      
      </div></td>
    </tr>
    <tr class="">
      <td>Orden:</td>
    <td><div align="center">
      <input name="orden" type="text" id="orden" class="input-block-level" onKeypress="return solo_numeros(event);"/>
    </div></td>
    </tr>
    <tr class="">
      <td>Tecla de Acceso Rapido:</td>
      <td><div align="center">
        <input name="tecla" type="text" id="tecla" class="input-block-level" />
      </div></td>
    </tr>
    <tr class="">
      <td>Estado:</td>
      <td><div align="center">
        <select name="estado" id="estado">
          <option value="1">Activo</option>
          <option value="0">No Activo</option>
        </select>
      </div></td>
    </tr>
    <tr class="">
      <td>Visible:</td>
      <td><div align="center">
        <select name="visible" id="visible">
          <option value="1">SI</option>
          <option value="0">NO</option>
                </select>
      </div></td>
    </tr>
    <tr class="">
      <td>Tipo de Menu: </td>
    <td><div align="center">
      <select name="tipo_menu" id="tipo_menu">
        <option value="1">Favorito</option>
        <option value="0">No Favorito</option>
            </select>
    </div></td>
    </tr>
    <tr class="">
      <td colspan="2"><div align="center">
      	<input title="Insertar Datos" class="btn btn-success " onClick="return validar_campos();" type="submit" value="Insertar">
        
        <input name="action" type="hidden" id="action" value="1" />
      </div></td>
      </tr>
  </table>
  </div>
  </div>
</form>

</body>
</html>
