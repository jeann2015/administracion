<?php
if (!isset($_SESSION)) 
{
	session_start();
	if($_SESSION['cod_usu']==""){$var_cod_usu = "";}else{$var_cod_usu = $_SESSION['cod_usu'];}
	if($_SESSION['action']==""){$var_accion = "0";}else{$var_accion = $_SESSION['action'];}	
	
}

include('db.php'); 
require('fpdf17/fpdf.php');

$conn = phpmkr_db_connect_principal(HOST2, USER2, PASS2, DB2, PORT2);
$conn2 = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);	
$var_empresas = usuario_empresa($var_cod_usu,$conn2);

if($var_cod_usu==''){devolver();}

if(isset($_GET['empresa']))
{	
	$var_empresa = $_GET['empresa'];
}
else
{
	$var_empresa="";
}



if(isset($_GET['num_auto']))
{	
	$var_num_auto = $_GET['num_auto'];
}
else
{
	$var_num_auto="";
}

if(isset($_GET['fecha_desde']) && isset($_GET['fecha_hasta']))
{
	$var_fecha_desde = $_GET['fecha_desde'];
	$var_fecha_hasta = $_GET['fecha_hasta'];
}
else
{
	$var_fecha_desde ="";$var_fecha_hasta ="";
}

class PDF extends FPDF
{



function Usuarios($header, $data,$desde,$hasta)
{
	$this->SetFont('Arial','B',12);
	$this->Cell(40,10,'Pagos Por Usuarios');
	$this->Ln();	
	$this->SetFont('Arial','',12);
	$this->Cell(105,7,'Usuario',1);	
	$this->Cell(20,7,'Monto',1);		
	$this->Ln();
	foreach($data as $row)
	{
	
		$this->Cell(105,6,substr($row['nombre_empresa'],0,100),1);
		$this->Cell(20,6,$row['efectivo'],1);		
		$this->Ln();
	}

}

function Diarios($data,$desde,$hasta,$empresa,$conn)
{
	if($empresa==0){$nombre_empresa = "TODOS";}

	$this->SetFont('Arial','B',12);
	$this->Cell(40,10,'Reporte de Tickets');
	$this->Ln();
	$this->Cell(40,10,'Empresa: '.$nombre_empresa);
	$this->Ln();
	$this->Cell(40,10,'Desde:'.$desde);
	$this->Cell(40,10,'Hasta:'.$hasta);
	$this->Ln();	
	$this->SetFont('Arial','B',8);
	$this->Cell(21,6,'Numero Ticket',1);	
	$this->Cell(13,6,'Auto',1);	
	$this->Cell(22,6,'Num. Operador',1);	
	$this->Cell(60,6,'Operador',1);	
	$this->Cell(60,6,'Empresa',1);	
	$this->Cell(20,6,'Fecha Imp',1);	
	$this->Cell(18,6,'Hora Imp',1);	
	$this->Cell(18,6,'Fecha Diario',1);	
	$this->Cell(15,6,'Monto Dia',1);	
	$this->Cell(21,6,'Monto Res.',1);	
	$this->Cell(17,6,'Kilometraje',1);	
	$this->Cell(15,6,'Tipo Pago',1);		
	$this->Cell(40,6,'Usuario',1);		
	$this->Ln();

	$var_monto_total_general=0;

	foreach($data as $row)
	{
		
		if($row['tipo_pago']==1){$var_tipo_pago = "Efectivo";}else{$var_tipo_pago = "Deposito";}

		$sSql="select CONCAT(nombre, ' ', apellido) as nombre from operadores where num_oper = ".$row['num_ope']."";
		$rs=phpmkr_query($sSql,$conn) 
		or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$nombre_operador = $row_rs['nombre'];}

		$sSql="select descripcion as nombre from empresas where codigo = ".$row['empresa']."";
		$rs=phpmkr_query($sSql,$conn) 
		or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$nombre_empresa = $row_rs['nombre'];}
 
		$sSql="select nombre from usuarios where codigo = ".$row['cod_usu']."";
		$rs=phpmkr_query($sSql,$conn) 
		or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
		while ($row_rs = $rs->fetch_assoc())
		{$nombre_usuarios = $row_rs['nombre'];}
		$this->SetFillColor(224,235,255);
    	$this->SetTextColor(0);
		$this->SetFont('Arial','B',10);
		$this->Cell(21,6,$row['numero_ticket'],1);	
		$this->Cell(13,6,$row['num_auto'],1);			
		$this->Cell(22,6,$row['num_ope'],1,'','C');
		$this->SetFont('Arial','',8);	
		$this->Cell(60,6,$nombre_operador,1);	
		$this->Cell(60,6,$nombre_empresa,1);	
		$this->Cell(20,6,fecha($row['fecha_impresion']),1);	
		$this->Cell(18,6,conversion_hora($row['hora_impresion']),1);	
		$this->Cell(18,6,fecha($row['fecha']),1);	
		$this->SetFont('Arial','B',11);
		$this->Cell(15,6,$row['monto_dia'],1);	
		$this->Cell(21,6,$row['monto_restante'],1);	
		$this->Cell(17,6,$row['kilometros'],1);	
		$this->SetFont('Arial','',8);
		$this->Cell(15,6,$var_tipo_pago,1);		
		$this->Cell(40,6,$nombre_usuarios,1);	
		$this->Ln();
		$var_monto_total_diario_general = $var_monto_total_diario_general+$row['monto_dia'];
		$var_monto_total_general = $var_monto_total_general+$row['monto_total_pagado'];
	}
	$this->SetFont('Arial','B',8);	
	$this->Cell(21,6,'',1);	
	$this->Cell(13,6,'',1);	
	$this->Cell(22,6,'',1);	
	$this->Cell(60,6,'',1);	
	$this->Cell(60,6,'',1);	
	$this->Cell(20,6,'',1);	
	$this->Cell(18,6,'',1);	
	$this->Cell(18,6,'Totales:',1);	
	//$this->Cell(15,6,$var_monto_total_diario_general,1);	
	$this->Cell(15,6,$var_monto_total_diario_general,1);	
	$this->Cell(21,6,'',1);
	$this->Cell(17,6,'',1);	
	$this->Cell(15,6,'',1);		
	$this->Cell(40,6,'',1);		
	$this->Ln();

}

}

$pdf = new PDF('L','mm','Legal');



if($var_empresa=="0" && $var_num_auto=="")
{$sSql="select * from tickets where fecha_impresion between '".fecha_sql($var_fecha_desde)."' 
and '".fecha_sql($var_fecha_hasta)."' and empresa in ".$var_empresas." order by num_auto,numero_ticket";}

if($var_empresa<>"0" && $var_num_auto<>"" )
{$sSql="select * from tickets where fecha_impresion between '".fecha_sql($var_fecha_desde)."' 
and '".fecha_sql($var_fecha_hasta)."' and num_auto = '".$var_num_auto."' and empresa = ".$var_empresa." order by num_auto,numero_ticket";}


if($var_empresa<>"0" && $var_num_auto=="" )
{$sSql="select * from tickets where fecha_impresion between '".fecha_sql($var_fecha_desde)."' 
and '".fecha_sql($var_fecha_hasta)."' and empresa = ".$var_empresa." order by num_auto,numero_ticket";}


if($var_num_auto<>"" && $var_empresa=="0")
{$sSql="select * from tickets where fecha_impresion between '".fecha_sql($var_fecha_desde)."' 
and '".fecha_sql($var_fecha_hasta)."'  and num_auto = '".$var_num_auto."' and empresa in ".$var_empresas." order by num_auto,numero_ticket";}

if($var_empresa=="0" && $var_num_auto=="")
{$sSql="select * from tickets where fecha_impresion between '".fecha_sql($var_fecha_desde)."' 
and '".fecha_sql($var_fecha_hasta)."' and empresa in ".$var_empresas."  order by num_auto,numero_ticket";}

//echo $sSql;
	$data_empresas = array();


	$rs_e=phpmkr_query($sSql,$conn) or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs_e = $rs_e->fetch_assoc())
	{$data_empresas[]=$row_rs_e;}

	//echo $sSql;

	//print "<pre>".print_r($data_empresas,true)."</pre>";die;


	if(phpmkr_num_rows($rs_e)>0)
	{
		$pdf->SetFont('Arial','',8);
		$pdf->AddPage();
		$pdf->Diarios($data_empresas,$_GET['fecha_desde'],$_GET['fecha_hasta'],$_GET['empresa'],$conn);	
		$pdf->Output();
	}



?>



