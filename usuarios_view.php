<?php
	if (!isset($_SESSION)) 
	{
		session_start();
		if($_SESSION['cod_usu']==""){$var_cod_usu = "";}else{$var_cod_usu = $_SESSION['cod_usu'];}
		if($_SESSION['action']==""){$var_accion = "0";}else{$var_accion = $_SESSION['action'];}		
	}
	include ("db.php"); 
	$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);
	$var_cod_usu=15;
	if($var_cod_usu==''){devolver();}
	
	$rs=phpmkr_query("select a.ver,a.insertar,a.modificar,a.eliminar from accesos a where a.cod_usu = $var_cod_usu and a.cod_men=5",$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_insertar = $row_rs['insertar'];
		$var_modificar = $row_rs['modificar'];
		$var_eliminar = $row_rs['eliminar'];
	}
?>
<head>
<?php validar_acciones(); ?>
	<!-- LH_COUNTRY -->
<title><?php echo nombre_aplicacion(); ?></title>	
<link href="assets/css/bootstrap.css" rel="stylesheet">	

</head>


<body onload="validar_acciones(<?php echo $var_accion; ?>)" class="lh">
<!-- WebReporting Common data --><script type="text/javascript"><!--
//-->
</script>
<script type="text/javascript" src="css/kb_shortcut.packed.js" ></script>
<?php teclas_salir(); ?>
<table width="887" border="0" class="table table-hover" align="center">
    <tr class="success">
      <td colspan="8"><strong>Modulo de Usuarios </strong></td>
    </tr>
    <tr class="">
      <td colspan="8"><a title="Regresar" href='principal.php' class='btn btn-success' ><i class='icon-chevron-left'></i>Menu Principal</a></td>
    </tr>
    <tr class="">
      <td colspan="8"><?php if($var_insertar=='1'){echo "<a title='Ingresar Datos' href='usuarios_add.php' class='btn btn-success' ><i class='icon-chevron-down'></i>Ingresar Nuevo Usuario</a>";}else{echo "<a href=''><img src='img/agregar2.png' width='14' height='14' border='0' /></a>";} ?></td>
    </ul></tr>
    <tr class="info">
      <td width="43"><div align="center"><strong>Codigo</strong></div></td>
      <td width="314"><div align="center"><strong>Nombre</strong></div></td>
      <td width="91"><div align="center"><strong>Usuario</strong></div></td>
      <td width="94"><div align="center"><strong>Estado</strong></div></td>
      <td width="140"><div align="center"><strong>Fecha de Creacion </strong></div></td>
      <td width="70"><div align="center"><strong>Modificar</strong></div></td>
      <td width="87"><div align="center"><strong>
              Eliminar
            </strong></div></td>
    </tr>
	<?php
	$var_color="dark";
	$rs=phpmkr_query("select * from usuarios where estado = 1 Order by codigo",$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_codigo=$row_rs['codigo'];
		$var_nombre=$row_rs['nombre'];
		$var_fecha=fecha($row_rs['fecha']);
		$var_usuario=$row_rs['usuario'];
		$var_estado=$row_rs['estado'];
		if($var_color=="dark"){$var_color="light";}else{$var_color="dark";}	
	?>
    <tr >
	  <td><div align="center"><?php echo $var_codigo; ?></div></td>
      <td><div align="center"><?php echo $var_nombre; ?></div></td>
      <td><div align="center"><?php echo $var_usuario; ?></div></td>
      <td><div align="center"><?php if($var_estado==1){echo "Activo";}else{echo "No Activo";} ?></div></td>
      <td><div align="center"><?php echo $var_fecha; ?></div></td>
      <td><div align="center"><?php if($var_modificar=='1'){ echo "<a title='Modificar' href='usuarios_mod.php?codigo=$var_codigo' class='btn btn-success' ><i class='icon-pencil'></i></a>";}else{echo "<a href='' class='btn btn-danger' ><i class='icon-pencil'></i></a>" ;} ?></td>
      <td><div align="center"><?php if($var_eliminar=='1') { echo "<a title='Eliminar' href='usuarios_eli.php?codigo=$var_codigo' class='btn btn-success' ><i class='icon-trash'></i></a>";}else{echo "<a href='' class='btn btn-danger' ><i class='icon-trash'></i></a>";} ?></td>

    </tr>
	<?php } $_SESSION['action']="0"; ?>
  </table>
</body>
</html>
