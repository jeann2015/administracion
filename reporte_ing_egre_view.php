<?php

	if (!isset($_SESSION)) 
	{
		session_start();
		if($_SESSION['cod_usu']==""){$var_cod_usu = "";}else{$var_cod_usu = $_SESSION['cod_usu'];}
		if($_SESSION['action']==""){$var_accion = "0";}else{$var_accion = $_SESSION['action'];}		
		$_SESSION['nombre_view']="reporte_ing_egre_view.php";
	}

	include ("db.php"); 
	$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);	
  $conn2 = phpmkr_db_connect_principal(HOST2, USER2, PASS2, DB2, PORT2);   
  if($var_cod_usu==''){devolver();}
  
  $var_empresas = usuario_empresa($var_cod_usu,$conn);
	$var_fecha_actual = fecha_aplicacion($conn);
  auditoria($var_cod_usu,'VIO EL REPORTE DE INGRESOS Y EGRESOS',$conn);

if(isset($_POST['empresa']))
{ 
  $var_empresa = $_POST['empresa'];
}
else
{
  $var_empresa="0";
}

if(isset($_POST['num_auto']))
{ 
  $var_num_auto = $_POST['num_auto'];
}
else
{
  $var_num_auto="";
}

if(isset($_POST['fecha_desde']) && isset($_POST['fecha_hasta']))
{
  $var_fecha_desde = $_POST['fecha_desde'];
  $var_fecha_hasta = $_POST['fecha_hasta'];
}
else
{
  $var_fecha_desde =fecha($var_fecha_actual);$var_fecha_hasta =fecha($var_fecha_actual);
}

  $rs=phpmkr_query("select a.ver,a.insertar,a.modificar,a.eliminar from accesos a where a.cod_usu = $var_cod_usu and a.cod_men=89",$conn) 
  or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
  while ($row_rs = $rs->fetch_assoc())
  {
    $var_insertar = $row_rs['insertar'];
    $var_modificar = $row_rs['modificar'];
    $var_eliminar = $row_rs['eliminar'];
  }
  $rs=phpmkr_query("select m.titulo from menus m where m.codigo2=89",$conn) 
  or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
  while ($row_rs = $rs->fetch_assoc())
  {$var_titulo = $row_rs['titulo'];}

  

?>

<head>


<title><?php echo nombre_aplicacion(); ?></title>	
<link href="assets/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript" src="lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="lib/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="lib/jscalendar/calendar-setup.js"></script>
<link href="lib/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css"/>

<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap-transition.js"></script>
<script src="assets/js/bootstrap-alert.js"></script>
<script src="assets/js/bootstrap-modal.js"></script>
<script src="assets/js/bootstrap-dropdown.js"></script>
<script src="assets/js/bootstrap-scrollspy.js"></script>
<script src="assets/js/bootstrap-tab.js"></script>
<script src="assets/js/bootstrap-tooltip.js"></script>
<script src="assets/js/bootstrap-popover.js"></script>
<script src="assets/js/bootstrap-button.js"></script>
<script src="assets/js/bootstrap-collapse.js"></script>
<script src="assets/js/bootstrap-carousel.js"></script>
<script src="assets/js/bootstrap-typeahead.js"></script>
<script src="assets/js/bootstrap-affix.js"></script>

</head>

<body onLoad="" class="lh">

<?php
/////////////////////////////////////Renta Diaria

if( $var_empresa=="0" && $var_num_auto=="")
{
  
  $sSql="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
  where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
  and '".fecha_sql($var_fecha_hasta)."' 
   Group by num_auto,num_ope";
}

if( $var_empresa<>"0" && $var_num_auto=="")
{
  
  $sSql="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
  where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
  and '".fecha_sql($var_fecha_hasta)."' and empresa = ".$var_empresa." 
  Group by num_auto,num_ope,empresa";
}
if( $var_empresa<>"0" && $var_num_auto<>"")
{
  
  $sSql="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
  where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
  and '".fecha_sql($var_fecha_hasta)."' and empresa = ".$var_empresa." and num_auto = '$var_num_auto' 
  Group by num_auto,num_ope,empresa";
}
if( $var_empresa=="0" && $var_num_auto<>"")
{
  
  $sSql="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
  where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
  and '".fecha_sql($var_fecha_hasta)."' and num_auto = '$var_num_auto' 
  Group by num_auto,num_ope,empresa";
}

    $con=1;
 
    $rs=phpmkr_query($sSql,$conn2) 
    or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
    while ($row_rs = $rs->fetch_assoc())
    {
      $tickets=$row_rs['monto_pagado'];
      $num_auto=$row_rs['num_auto'];
      $num_ope=$row_rs['num_ope'];
      $empresa=$row_rs['empresa'];



?>

<div id='renta<?php echo $num_auto; ?>' class='modal hide fade' tabindex='' width='' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
<div class='modal-header'>
<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
<h4 id='myModalLabel'>Detalle de Renta Diaria</h4>
<h4 id='myModalLabel'>Auto:<?php echo $num_auto; ?></h4>
</div>
<div class='modal-body'>
<table class='table table-hover' border='0'>
  <tr class='info'>    
    <td title='Fecha de Impresion de Ticket'>
      Fecha de Impresion
    </td>    
    <td>
      Monto Pagado
    </td>
    <td>
      Kilometraje
    </td>
  </tr>

<?php 
    
    $sSql="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa,fecha_impresion,fecha,kilometros from tickets 
    where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' and empresa = ".$empresa." and num_auto = '".$num_auto."' 
    group by fecha_impresion order by fecha desc";
    $var_total=0;
    $rs2=phpmkr_query($sSql,$conn2) 
    or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
    while ($row_rs2 = $rs2->fetch_assoc())
    {
      $var_fecha=fecha($row_rs2['fecha']);
      $var_fecha_impresion=fecha($row_rs2['fecha_impresion']);
      $var_monto_pagado=$row_rs2['monto_pagado'];
      $var_kilometraje=$row_rs2['kilometros'];
      $var_total=$var_total+$var_monto_pagado;
	 
?>
  <tr>
    
    <td title='Fecha de Impresion de Ticket'>
     <?php echo $var_fecha_impresion; ?>
    </td>
    <td>
     <?php echo $var_monto_pagado; ?>
    </td>
    <td>
    <?php echo $var_kilometraje; ?>
    </td>
  </tr>
  
  <?php } ?>
  <tr>
    <td align='right' colspan='1'>
      <b>Total:</b>
    </td>
    <td colspan='2'>
      <b><?php echo number_format($var_total,2); ?></b>
    </td>
  </tr>
</table>
</div>
<div class='modal-footer'>

<button class='btn' data-dismiss='modal'>Cerrar</button>
</div>
</div>

<div id='caja<?php echo $num_auto; ?>' class='modal hide fade' tabindex='' width='' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
<div class='modal-header'>
<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
<h4 id='myModalLabel'>Detalle de Caja Menuda</h4>
<h4 id='myModalLabel'>Auto:<?php echo $num_auto; ?></h4>
</div>
<div class='modal-body'>
<table class='table table-hover' border='0'>
  <tr class='info'>
    
    <td title='Fecha de Registro de Gasto de Caja Menuda'>
     Fecha
    </td>
    <td>
      Monto 
    </td>
     <td>
      Factura 
    </td>
    <td>
      Detalle
    </td>
  </tr>
<?php 
    $var_total=0;
    $sSql="select monto as monto_caja_menuda,fecha,detalle,numero_factura from caja_menuda_hist
    where num_und = '".$num_auto."' and fecha between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' order by fecha asc";
    $var_total=0;
    $rs2=phpmkr_query($sSql,$conn2) 
    or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
    while ($row_rs2 = $rs2->fetch_assoc())
    {
      $var_fecha=fecha($row_rs2['fecha']);
      $var_detalle=$row_rs2['detalle'];
      $var_monto=$row_rs2['monto_caja_menuda'];
      $var_numero_factura=$row_rs2['numero_factura'];
      $var_total=$var_total+$var_monto;
	 
?>
  <tr>
    
    <td title='Fecha de Registro de Gasto de Caja Menuda'>
     <?php echo $var_fecha; ?>
    </td>
    <td>
     <?php echo $var_monto; ?>
    </td>
    <td><?php echo $var_numero_factura; ?></td>
    <td>
    <?php echo $var_detalle; ?>
    </td>
  </tr>
  
  <?php } ?>
  <tr>
    <td align='right' colspan='1'>
      <b>Total:</b>
    </td>
    <td colspan='3'>
      <b><?php echo number_format($var_total,2); ?></b>
    </td>
  </tr>
</table>
</div>
<div class='modal-footer'>

<button class='btn' data-dismiss='modal'>Cerrar</button>
</div>
</div>

<div id='inve<?php echo $num_auto; ?>' class='modal hide fade' tabindex='' width='' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
<div class='modal-header'>
<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
<h4 id='myModalLabel'>Detalle de Salidas de Inventario</h4>
<h4 id='myModalLabel'>Auto:<?php echo $num_auto; ?></h4>
</div>
<div class='modal-body'>
<table class='table table-hover' border='0'>
  <tr class='info'>
    
    <td title='Fecha de Registro de Salida de Inventario'>
     Fecha
    </td>
   
    <td>
      Monto 
    </td>
     <td>
      Cantidad 
    </td>
    <td>
      Factura 
    </td>
    <td>
      Fue Cobrado? 
    </td>
    <td>
      Detalle
    </td>
  </tr>
<?php 
    $var_total=0;
  $sSql="select sum(monto) as monto_salida,p.descripcion,fecha,cobrar,cantidad,factura from salidas s,productos p 
      where s.num_und = '".$num_auto."' and s.fecha between '".fecha_sql($var_fecha_desde)."'   
      and '".fecha_sql($var_fecha_hasta)."' and s.cod_pro = p.codigo Group by p.descripcion,fecha,cobrar,cantidad,factura";
	  
    $var_total=0;
    $rs2=phpmkr_query($sSql,$conn2) 
    or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
    while ($row_rs2 = $rs2->fetch_assoc())
    {
      $var_fecha=fecha($row_rs2['fecha']);
      $var_descripcion=$row_rs2['descripcion'];
	  if($row_rs2['fecha']>='2013-09-30')
	  {
      	$var_monto_imp=number_format(($row_rs2['monto_salida']*7)/100, 2, '.', '');
      	$var_monto=number_format($row_rs2['monto_salida']+$var_monto_imp,2,'.','');
	  }
	  else
	  {
	  	$var_monto=$row_rs2['monto_salida'];
	  }
      $var_cobrar=$row_rs2['cobrar'];
      $var_cantidad=$row_rs2['cantidad'];
      $var_factura=$row_rs2['factura'];
      $var_total=$var_total+$var_monto;
      $var_descripcion=$row_rs2['descripcion'];
?>
  <tr>
    
    <td title='Fecha de Registro de Salida de Inventario'>
     <?php echo $var_fecha; ?>
    </td>
    <td>
     <?php echo $var_monto; ?>
    </td>
    <td>
     <?php echo $var_cantidad; ?>
    </td>
    <td>
     <?php echo $var_factura; ?>
    </td>
    
     <td>
     <?php if($var_cobrar=='1'){echo "Si";}else{echo "No";} ?>
    </td>
    <td>
    <?php echo $var_descripcion; ?>
    </td>
  </tr>
  
  <?php } ?>
  <tr>
    <td align='right' colspan='1'>
      <b>Total:</b>
    </td>
    <td colspan='5'>
      <b><?php echo number_format($var_total,2); ?></b>
    </td>
  </tr>
</table>
</div>
<div class='modal-footer'>

<button class='btn' data-dismiss='modal'>Cerrar</button>
</div>
</div>
<?php 
//////////////////////////////////////////////////Renta Diaria
} 

?>

<script type="text/javascript" src="css/kb_shortcut.packed.js" ></script>
<?php teclas_salir(); ?>
<form name="form1" action="reporte_ing_egre_view.php" method="post">			
  <table width="50%" border="0" class="table table-hover" align="center">
    <tr class="success">
      <td colspan="10" align="center"><strong><?php echo $var_titulo; ?> </strong></td>
    </tr>
    <tr class="col31">
      <td colspan="11"><a title="Regresar" href='principal.php' class='btn btn-success' ><i class='icon-chevron-left'></i>Menu Principal</a></td>
    </tr>
    <tr class="col32">
      <td colspan="10">    <div align="center">
        <table width="50%" border="0" align="center">        	
          <tr  class="">
          	  <td colspan='2' width="315"><div id="contenedor" align="center">Empresa:
              <?php echo select2("codigo", "descripcion", $_POST['empresa'], "select codigo,descripcion from empresas where codigo in ".$var_empresas." order by codigo", "empresa",3,'','',$conn2); ?>			  
			 
			 
            </div></td>          	          
          </tr>
          <tr  class="">
              <td colspan='2' width="315"><div id="contenedor" align="center">Numero Auto:
              <input class="input-block-level"  name="num_auto" id="num_auto" type="text" class="textbox" value="<?php echo $_POST['num_auto']; ?>" size="15" maxlength="20" />              
        
       <input title="Buscar Datos" class="btn btn-success " onClick="" type="submit" value="Buscar">
       
            </div></td>                     
          </tr>        
          <tr>          	
          	<td colspan=''>Desde</td>
          	<td colspan=''>Hasta</td>
          </tr>          
          <tr>          	
          	<td align='center'>
          		<input class="input-block-level"  required='true' name="fecha_desde" id="fecha_desde" type="text" class="textbox" 
          		value="<?php if(isset($_POST['fecha_desde'])=='') {echo fecha($var_fecha_actual);} else {echo $_POST['fecha_desde']; } ?>" size="15" maxlength="20" />
              <input name="image7" type="image" id="image7" src="lib/jscalendar/cal.gif" />
          		<script type="text/javascript"> Calendar.setup( {inputField:"fecha_desde",ifFormat:"%d/%m/%Y",button:"image7",firstDay:1,weekNumbers:false,showOthers:true} );</script>
          	</td>
            
          	<td align='center'>
          		<input class="input-block-level" required='true' name="fecha_hasta" id="fecha_hasta" type="text" class="textbox" 
          		value="<?php if(isset($_POST['fecha_hasta'])=='') {echo fecha($var_fecha_actual);} else {echo $_POST['fecha_hasta']; } ?>" size="15" maxlength="20" />          	
          		<input name="image8" type="image" id="image8" src="lib/jscalendar/cal.gif" />
          		<script type="text/javascript"> Calendar.setup( {inputField:"fecha_hasta",ifFormat:"%d/%m/%Y",button:"image8",firstDay:1,weekNumbers:false,showOthers:true} );</script>
          	</td>
          </tr>
          
        </table>
    </div>    
</tr>
</table>

<table class="table table-hover" border='0' width='100%'>
  <tr  class='info'>
    <td align='center'>Item</td>    
    <td align='center'>Unidad</td>    
    <td align='center'>Operador</td>    
    <td align='center'>Monto Pagado</td>    
    <td align='center'>Gasto Caja Menuda</td>    
    <td align='center'>Gasto Inventario</td>    
    <td align='center'>Saldo</td> 
  </tr>
<?php 
  
  

  if($var_empresa=="0" && $var_num_auto=="")
  {
    
    $sSql_t="select sum(monto_dia) as monto_pagado,num_auto,num_ope from tickets 
    where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' 
    and num_auto in (select num_und from autos where cod_emp in ".$var_empresas.") Group by num_auto,num_ope,empresa";
  }
  if($var_empresa<>"0" && $var_num_auto=="")
  {
    
    $sSql_t="select sum(monto_dia) as monto_pagado,num_auto,num_ope from tickets 
    where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' 
    and num_auto in (select num_und from autos where cod_emp in (".$var_empresa.")) Group by num_auto";
  }
  if( $var_empresa<>"0" && $var_num_auto<>"")
  {
    
    $sSql_t="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
    where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' and empresa = ".$var_empresa." and num_auto = '$var_num_auto' 
    Group by num_auto,num_ope,empresa";
  }
  if( $var_empresa=="0" && $var_num_auto<>"")
  {
    
    $sSql_t="select sum(monto_dia) as monto_pagado,num_auto,num_ope,empresa from tickets 
    where fecha_impresion between '".fecha_sql($var_fecha_desde)."'   
    and '".fecha_sql($var_fecha_hasta)."' and num_auto = '$var_num_auto' 
    Group by num_auto,num_ope,empresa";
  }

    $con=1;
    $var_tickets=0;
    $var_monto_caja_menuda=0;
    $var_total_saldo=0;
    $monto_descontar_total=0;
	
   //echo $sSql_t;
    $var_num_und_comparar=0;

    $rs=phpmkr_query($sSql_t,$conn2) 
    or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
    while ($row_rs = $rs->fetch_assoc())
    {
      $tickets  = $row_rs['monto_pagado'];
      $num_auto = $row_rs['num_auto'];
      $num_ope  = $row_rs['num_ope'];

      $sSql="select concat(nombre,' ',apellido) as nombre from operadores
      where num_oper = ".$num_ope."";
      $rs_ope=phpmkr_query($sSql,$conn2) 
      or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
      while ($row_rs_ope = $rs_ope->fetch_assoc())
      {
        $nombre=$row_rs_ope['nombre'];
      }

      if($var_num_und_comparar<> $num_auto)
      {
      
      $monto_caja_menuda=0;

      $sSql="select sum(monto) as monto_caja_menuda from caja_menuda_hist
      where num_und = '".$num_auto."' and fecha between '".fecha_sql($var_fecha_desde)."'   
      and '".fecha_sql($var_fecha_hasta)."'";
      $rs_gas=phpmkr_query($sSql,$conn2) 
      or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
      while ($row_rs_gas = $rs_gas->fetch_assoc())
      {
        $monto_caja_menuda=$row_rs_gas['monto_caja_menuda'];
      }
      $monto_salida=0;
      $sSql="select monto as monto_salida,cobrar from salidas
      where num_und = '".$num_auto."' and fecha between '".fecha_sql($var_fecha_desde)."'   
      and '".fecha_sql($var_fecha_hasta)."'";
      $rs_sal=phpmkr_query($sSql,$conn2) 
      or die("Fallo al ejecutar la consulta en la linea" . __LINE__ . ": " . phpmkr_error($conn2) . '<br>SQL: ' . $sSql);
      while ($row_rs_sal = $rs_sal->fetch_assoc())
      {
        //$monto_salida=$row_rs_sal['monto_salida'];
        $cobrar=$row_rs_sal['cobrar'];
        if($cobrar==1)
        {
          $monto_salida=$monto_salida+$row_rs_sal['monto_salida'];
        }
      }
      $var_num_und_comparar=$num_auto;
      }
      else
      {
         $monto_caja_menuda=0;
         $monto_caja_menuda=0;
         $monto_salida=0;
      }
      $monto_imp = ($monto_salida*7)/100;
	    $monto_salida=$monto_salida+$monto_imp;
      $monto_descontar = $monto_caja_menuda+$monto_salida;
      
      $saldo = $tickets-$monto_descontar;
      $total_general=$total_general+$saldo;
	    $var_tickets= $var_tickets+$tickets;
	    $var_monto_caja_menuda = $var_monto_caja_menuda+$monto_caja_menuda;
	    $var_total_saldo= $var_total_saldo+$saldo;
	    $monto_descontar_total=$monto_descontar_total+$monto_descontar;
?>
  <tr>
    <td align='center'><?php echo $con; ?></td>    
    <td align='center'><?php echo $num_auto; ?></td>    
    <td align='center'><?php echo $nombre; ?></td>    
    <td align='center'><a title='Ver Detalles' data-toggle="modal" href="#renta<?php echo $num_auto; ?>" class="btn btn-success btn-large"><?php echo number_format($tickets,2); ?></a></td>    
    <td align='center'><a title='Ver Detalles' data-toggle="modal" href="#caja<?php echo $num_auto; ?>" class="btn btn-success btn-large"><?php echo number_format($monto_caja_menuda,2); ?></a></td>    
    <td align='center'><a title='Ver Detalles' data-toggle="modal" href="#inve<?php echo $num_auto; ?>" class="btn btn-success btn-large"><?php echo number_format($monto_salida,2); ?></a></td>    
    <td align='center'><b><?php echo number_format($saldo,2); ?></b></td> 
  </tr>
<?php $con=$con+1;} ?>
<tr>
 <td colspan='' ><div align='left'></div></td>
  <td colspan='' ><div align='left'></div></td>
  <td colspan='' ><div align='right'>Total General:</div></td>
  <td colspan='' ><div align='left'><b><?php echo number_format($var_tickets,2); ?></b></div></td>
  <td colspan='' ><div align='left'><b><?php echo number_format($var_monto_caja_menuda,2); ?></b></div></td>
  <td colspan='' ><div align='left'><b><?php echo number_format($monto_descontar_total,2); ?></b></div></td>
  <td colspan='' ><div align='left'><b><?php echo number_format($var_total_saldo,2); ?></b></div></td>
  
</tr>
</table>

</form>
</body>
</html>