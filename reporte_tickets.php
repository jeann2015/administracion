<?php
	if (!isset($_SESSION)) 
	{
		session_start();
		if($_SESSION['cod_usu']==""){$var_cod_usu = "";}else{$var_cod_usu = $_SESSION['cod_usu'];}
		if($_SESSION['action']==""){$var_accion = "0";}else{$var_accion = $_SESSION['action'];}		
		$_SESSION['nombre_view']="reporte_tickets.php";
	}
	include ("db.php"); 
	$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);	
  $conn2 = phpmkr_db_connect_principal(HOST2, USER2, PASS2, DB2, PORT2);  
  $var_empresas = usuario_empresa($var_cod_usu,$conn);

  auditoria($var_cod_usu,'VIO EL REPORTE DE TICKETS',$conn);

	if($var_cod_usu==''){devolver();}

	$var_fecha_actual = fecha_aplicacion($conn);

  $rs=phpmkr_query("select a.ver,a.insertar,a.modificar,a.eliminar from accesos a where a.cod_usu = $var_cod_usu and a.cod_men=58",$conn) 
  or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
  while ($row_rs = $rs->fetch_assoc())
  {
    $var_insertar = $row_rs['insertar'];
    $var_modificar = $row_rs['modificar'];
    $var_eliminar = $row_rs['eliminar'];
  }
  $rs=phpmkr_query("select m.titulo from menus m where m.codigo2=58",$conn) 
  or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
  while ($row_rs = $rs->fetch_assoc())
  {$var_titulo = $row_rs['titulo'];}

  

?>

<head>


<title><?php echo nombre_aplicacion(); ?></title>	
<link href="assets/css/bootstrap.css" rel="stylesheet">

<script type="text/javascript" src="lib/jscalendar/calendar.js"></script>
<script type="text/javascript" src="lib/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="lib/jscalendar/calendar-setup.js"></script>
<link href="lib/jscalendar/calendar-blue.css" rel="stylesheet" type="text/css"/>


</head>

<body onLoad="" class="lh">
<script type="text/javascript" src="css/kb_shortcut.packed.js" ></script>
<?php teclas_salir(); ?>
<form name="form1" action="reporte_tickets.php" method="post">			
  <table width="50%" border="0" class="table table-hover" align="center">
    <tr class="success">
      <td colspan="10" align="center"><strong><?php echo $var_titulo; ?> </strong></td>
    </tr>
    <tr class="col31">
      <td colspan="11"><a title="Regresar" href='principal.php' class='btn btn-success' ><i class='icon-chevron-left'></i>Menu Principal</a></td>
    </tr>
    <tr class="col32">
      <td colspan="10">    <div align="center">
        <table width="50%" border="0" align="center">        	
          <tr  class="">
          	  <td colspan='2' width="315"><div id="contenedor" align="center">Empresa:
              <?php echo select2("codigo", "descripcion", $_POST['empresa'], "select codigo,descripcion from empresas where codigo in ".$var_empresas." order by codigo", "empresa",3,'','',$conn2); ?>			  
			 
			 
            </div></td>          	          
          </tr>
          <tr  class="">
              <td colspan='2' width="315"><div id="contenedor" align="center">Numero Auto:
              <input class="input-block-level"  name="num_auto" id="num_auto" type="text" class="textbox" value="<?php echo $_POST['num_auto']; ?>" size="15" maxlength="20" />              
        
       <input title="Buscar Datos" class="btn btn-success " onClick="" type="submit" value="Buscar">
       
            </div></td>                     
          </tr>
          
          <tr>          	
          	<td colspan=''>Desde</td>
          	<td colspan=''>Hasta</td>
          </tr>          
          <tr>          	
          	<td align='center'>
          		<input class="input-block-level"  required='true' name="fecha_desde" id="fecha_desde" type="text" class="textbox" 
          		value="<?php if(isset($_POST['fecha_desde'])=='') {echo fecha($var_fecha_actual);} else {echo $_POST['fecha_desde']; } ?>" size="15" maxlength="20" />
              <input name="image7" type="image" id="image7" src="lib/jscalendar/cal.gif" />
          		<script type="text/javascript"> Calendar.setup( {inputField:"fecha_desde",ifFormat:"%d/%m/%Y",button:"image7",firstDay:1,weekNumbers:false,showOthers:true} );</script>
          	</td>
            
          	<td align='center'>
          		<input class="input-block-level" required='true' name="fecha_hasta" id="fecha_hasta" type="text" class="textbox" 
          		value="<?php if(isset($_POST['fecha_hasta'])=='') {echo fecha($var_fecha_actual);} else {echo $_POST['fecha_hasta']; } ?>" size="15" maxlength="20" />          	
          		<input name="image8" type="image" id="image8" src="lib/jscalendar/cal.gif" />
          		<script type="text/javascript"> Calendar.setup( {inputField:"fecha_hasta",ifFormat:"%d/%m/%Y",button:"image8",firstDay:1,weekNumbers:false,showOthers:true} );</script>
          	</td>
          </tr>
          
        </table>
    </div>    
</tr>
</table>
<?php 
if(isset($_POST['fecha_hasta']))
{
?>
<table height='60%' width = '100%'>
<tr>          	

<td  colspan=''>
	<iframe width='100%' height='100%' src="reporte_tickets_pdf.php?fecha_desde=<?php echo $_POST['fecha_desde']; ?>&fecha_hasta=<?php echo $_POST['fecha_hasta']; ?>&empresa=<?php echo $_POST['empresa']; ?>&num_auto=<?php echo $_POST['num_auto']; ?>"></iframe>
</td>
</tr>
</table>
<?php 
}
?>
</form>
</body>
</html>