<?php 
if (!isset($_SESSION)) 
{
  session_start();
}

include ("db.php");  
$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
   <title><?php echo nombre_aplicacion(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="shortcut icon" href="assets/ico/favicon.png">
  </head>

  <body>

    <div class="container">

      <form name="form1" id="form1" class="form-signin" action="validacion_usuarios.php" method="post">
      	<h3 align="center" class="form-signin-heading"><?php echo nombre_aplicacion(); ?></h3>
        <h2 align="center" class="form-signin-heading">Acceso</h2>
        <input  type="text" language="" required="true" name="usuario" autofocus="true"  class="input-block-level" placeholder="Usuario">
        <input type="password" name="clave" required="true" class="input-block-level" placeholder="Clave">
        <button name="entrar" class="btn btn-large btn-success" type="submit">Entrar</button>
      </form>
	<div align="center" class="container-fluid">
	<span class="badge">&copy; JEAN CARLOS NUNEZ HERNANDEZ, BAJO REGISTRO:2013-304</span><br>
  <span class="badge">Licencia Autorizada a: Word Taxi Administracion</span>
	
	<?php
	
	//print_r($_SESSION)."Hola"; 
	if(isset($_SESSION['accion']))
	{
	if($_SESSION['accion']=='0')
	{
	unset($_SESSION['accion']);
	
	echo "<div class='alert alert-block'>
   	<h4>Error!</h4>
   	Este Usuario esta desactivado o Ingreso mal la Clave, Intente de Nuevo.
    </div>";
	
	}}
	?>
	
	</div>
    </div> 
    
    
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap-transition.js"></script>
    <script src="assets/js/bootstrap-alert.js"></script>
    <script src="assets/js/bootstrap-modal.js"></script>
    <script src="assets/js/bootstrap-dropdown.js"></script>
    <script src="assets/js/bootstrap-scrollspy.js"></script>
    <script src="assets/js/bootstrap-tab.js"></script>
    <script src="assets/js/bootstrap-tooltip.js"></script>
    <script src="assets/js/bootstrap-popover.js"></script>
    <script src="assets/js/bootstrap-button.js"></script>
    <script src="assets/js/bootstrap-collapse.js"></script>
    <script src="assets/js/bootstrap-carousel.js"></script>
    <script src="assets/js/bootstrap-typeahead.js"></script>

  </body>
</html>
<script>
function solo_enter(e) 
{ 
	var doc = document.form1;
	
    tecla = (document.all) ? e.keyCode : e.which;	
    if (tecla==13) 
	{doc.clave.focus(); return false;}else{return true;}
    patron =/\d/; 
    te = String.fromCharCode(tecla); 
    return patron.test(te); 
}	
	
</script>
