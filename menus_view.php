<?php
	if (!isset($_SESSION)) 
	{
		session_start();
		if($_SESSION['cod_usu']==""){$var_cod_usu = "";}else{$var_cod_usu = $_SESSION['cod_usu'];}
		if($_SESSION['action']==""){$var_accion = "0";}else{$var_accion = $_SESSION['action'];}		
	}
	include ("db.php"); 
	$conn = phpmkr_db_connect(HOST, USER, PASS, DB, PORT);
	
	if($var_cod_usu==''){devolver();}
	
	$rs=phpmkr_query("select a.ver,a.insertar,a.modificar,a.eliminar from accesos a where a.cod_usu = $var_cod_usu and a.cod_men=4",$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_insertar = $row_rs['insertar'];
		$var_modificar = $row_rs['modificar'];
		$var_eliminar = $row_rs['eliminar'];
	}
?>
<head>
<?php validar_acciones(); ?>
	
<title><?php echo nombre_aplicacion(); ?></title>	

</head>
<link href="assets/css/bootstrap.css" rel="stylesheet">
<body onLoad="validar_acciones(<?php echo $var_accion; ?>)" class="lh">
</script>
<script type="text/javascript" src="css/kb_shortcut.packed.js" ></script>
<?php teclas_salir(); ?>
<table width="961" border="0" class="table table-hover" align="center">
    <tr class="success">
      <td align="center" colspan="9"><strong>Modulo de Menus </strong></td>
    </tr>
    <tr align="center">
      <td colspan="9"><a title="Regresar" href='principal.php' class='btn btn-success' ><i class='icon-chevron-left'></i>Menu Principal</a></td>
    </tr>
    <tr  >
      <td align="center"  colspan="9"> 
        <?php if($var_insertar=='1'){echo "<a title='Ingresar Datos' href='menus_add.php' class='btn btn-success' ><i class='icon-chevron-down'></i>Ingresar Nuevo Menu</a>";} ?>
        
      </td></tr>
    <tr class="info">
      <td width="43"><div align="center"><strong>Codigo</strong></div></td>
      <td width="79"><div align="center"><strong>Codigo Padre</strong></div></td>
      <td width="71"><div align="center"><strong>Descripcion</strong></div></td>
      <td width="101"><div align="center"><strong>Archivo</strong></div></td>
      <td width="240"><div align="center"><strong>Titulo</strong></div></td>
      <td width="162"><div align="center"><strong>Mensaje</strong></div></td>
      <td width="39"><div align="center"><strong>Orden</strong></div></td>
      <td width="80"><div align="center"><strong>Modificar</strong></div></td>
      <td width="86"><div align="center"><strong>
              Eli minar
            </strong></div></td>
    </tr>
	<?php
	$rs=phpmkr_query("select * from menus Order by orden",$conn) or die("Fallo al ejecutar la consulta en la l?nea" . __LINE__ . ": " . phpmkr_error($conn) . '<br>SQL: ' . $sSql);
	while ($row_rs = $rs->fetch_assoc())
	{
		$var_codigo=$row_rs['codigo'];
		$var_cod_padre=$row_rs['cod_padre'];
		$var_descripcion=substr($row_rs['descripcion'],0,10);
		$var_archivo=$row_rs['archivo'];
		$var_titulo=$row_rs['titulo'];
		$var_mensaje=substr($row_rs['mensaje'],0,15);
		$var_orden=$row_rs['orden'];
		$var_codigo2=$row_rs['codigo2'];
		if($var_color=="" || $var_color == "light"){$var_color = "dark";}else{$var_color = "light";}	
	?>
    <tr class="<?php echo $var_color; ?>">
	  <td><div align="center"><?php echo $var_codigo; ?></div></td>
      <td><div align="center"><?php echo $var_cod_padre; ?></div></td>
      <td><div align="center"><?php echo $var_descripcion; ?></div></td>
      <td><div align="center"><?php echo $var_archivo; ?></div></td>
      <td><div align="center"><?php echo $var_titulo; ?></div></td>
      <td><div align="center"><?php echo $var_mensaje; ?></div></td>
      <td><div align="center"><?php echo $var_orden; ?></div></td>
      <td><div align="center">
        <?php if($var_modificar=='1'){ echo "<a title='Modificar' href='menus_mod.php?codigo=$var_codigo2' class='btn btn-success' ><i class='icon-pencil'></i></a>";}else{echo "<a href='' class='btn btn-danger' ><i class='icon-pencil'></i></a>" ;} ?>
        	
      </div></td>
      <td><div align="center">
        <?php if($var_eliminar=='1') { echo "<a title='Eliminar' href='menus_eli.php?codigo=$var_codigo2' class='btn btn-success' ><i class='icon-trash'></i></a>";}else{echo "<a href='' class='btn btn-danger' ><i class='icon-trash'></i></a>";} ?>
      </div></td>

    </tr>
	<?php } $_SESSION['action']=0;?>
  </table>


</body>
</html>
